namespace RedWorld.CharacterStats
{

    public enum AttributeModType
    {
        Flat = 100,
        PrecentAdd = 200,
        PrecentMult = 300,
    }
    public class AttributeModifier
    {
        //public Character character;

        public readonly float Value;
        public readonly AttributeModType Type;
        public readonly int Order;
        public readonly object Source;

        public AttributeModifier(float value, AttributeModType type, int order, object source)
        {
            Value = value;
            Type = type;
            Order = order;
        }
        public AttributeModifier(float value, AttributeModType type) : this(value, type, (int)type, null) { }
        public AttributeModifier(float value, AttributeModType type, int order) : this(value, type, order, null) { }
        public AttributeModifier(float value, AttributeModType type, object source) : this(value, type, (int)type, source) { }

    }

}
