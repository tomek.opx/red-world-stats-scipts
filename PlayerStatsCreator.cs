using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RedWorld.CharacterStats;

namespace Game.StatSystem
{
    //[CreateAssetMenu(fileName = "New PlayerInfo", menuName = "Character/PlayerInfo")] // test ze scriptable objects

    public class PlayerStatsCreator : ScriptableObject
    {
        [SerializeField]
        private Character character;

        public int statPoints = 10;
        public int baseLevel = 0;
        public int currentExp = 0;
        public int LastExpToNextLvl = 100;
        public int CurrentExpToNextLvl = 100;
        public int NextExpToNextLvl = 0;
        protected int pLevel;
        public float str, agi, con, inte, cha, wil;
        public int playerLevel
        {
            get
            {
                pLevel = baseLevel;
                return pLevel;
            }

        }
        public void AddExp(int exp)
        {
            currentExp += exp;
            if (currentExp >= CurrentExpToNextLvl)
            {
                currentExp -= CurrentExpToNextLvl;
                NextExpToNextLvl = LastExpToNextLvl + CurrentExpToNextLvl;
                LastExpToNextLvl = CurrentExpToNextLvl;
                CurrentExpToNextLvl = NextExpToNextLvl;
                LevelUp();
            }
            Debug.Log("dodaje " + exp + " expa. Posiadasz " + currentExp + " expa.");
            Debug.Log("exp do nastepnego poziomu wynosi: " + CurrentExpToNextLvl);

        }

        public void LevelUp()
        {
            baseLevel++;
            statPoints++;
            Debug.Log("Zyskales poziom. Twoj level to :" + playerLevel);

        }

        public void AddStr()
        {

            if (statPoints > 0)
            {
                Debug.Log("dodaje sily.");

               float value = character.Strength.BaseValue;

                if (value == 3)
                {
                    //character.Strength.BaseValue++;
                    str++;
                    character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValuMinusOne;
                    character.Range.BaseValue = character.Range.ValuMinusOne;
                    character.Blow.BaseValue = character.Blow.ValuMinusOne;
                    statPoints--;



                }
                else if (value == 4)
                {
                    //character.Strength.BaseValue++;
                    str++;
                    character.ShotAccuracy.BaseValue = character.ShotAccuracy.StartValue;
                    character.Range.BaseValue = character.Range.StartValue;

                    character.Blow.BaseValue = character.Blow.StartValue;
                    statPoints--;


                }
                else if (value >= 5)
                {
                    //character.Strength.BaseValue++;
                    str++;
                    character.ShotAccuracy.BaseValue += character.ShotAccuracy.ValuePerPoint1;
                    character.Range.BaseValue += character.Range.ValuePerPoint1;

                    character.Blow.BaseValue += character.Blow.ValuePerPoint1;
                    statPoints--;


                }

            }
            else
            {
                Debug.Log("Nie masz punktow statystyk");
            }
        }
        public void AddAgi()
        {
            if (statPoints > 0)
            {
                Debug.Log("dodaje zrecznosci");

                float value = character.Agility.BaseValue;

                if (value == 3)
                {
                    character.Agility.BaseValue++;
                    character.Hit.BaseValue = character.Hit.ValuMinusOne;
                    character.Range.BaseValue = character.Range.ValuMinusOne;
                    character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValuMinusOne;
                    character.Dodges.BaseValue = character.Dodges.ValuMinusOne;
                    character.Sneaking.BaseValue = character.Sneaking.ValuMinusOne;
                    statPoints--;



                }
                else if (value == 4)
                {
                    character.Agility.BaseValue++;
                    character.Hit.BaseValue = character.Hit.StartValue;
                    character.Range.BaseValue = character.Range.StartValue;
                    character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.StartValue;
                    character.Dodges.BaseValue = character.Dodges.StartValue;
                    character.Sneaking.BaseValue = character.Sneaking.StartValue;
                    statPoints--;


                }
                else if (value >= 5)
                {
                    character.Agility.BaseValue++;
                    character.Hit.BaseValue += character.Hit.ValuePerPoint1;
                    character.Range.BaseValue += character.Range.ValuePerPoint2;
                    character.ThrowingAccuracy.BaseValue += character.ThrowingAccuracy.ValuePerPoint1;
                    character.Dodges.BaseValue += character.Dodges.ValuePerPoint1;
                    character.Sneaking.BaseValue += character.Sneaking.ValuePerPoint1;
                    statPoints--;


                }

            }
            else
            {
                Debug.Log("Nie masz punktow statystyk");
            }
        }
        public void AddCon()
        {
            if (statPoints > 0)
            {
                Debug.Log("dodaje kondycjii");

                float value = character.Condition.BaseValue;

                if (value == 3)
                {
                    character.Condition.BaseValue++;
                    character.Healing.BaseValue = character.Healing.ValuMinusOne;
                    character.Defence.BaseValue = character.Defence.ValuMinusOne;
                    character.HPstart.BaseValue = character.HPstart.ValuMinusOne;
                    character.HPlvl.BaseValue = character.HPlvl.ValuMinusOne;
                    character.Poisoning.BaseValue = character.Poisoning.ValuMinusOne;
                    character.Dazzle.BaseValue = character.Dazzle.ValuMinusOne;
                    statPoints--;



                }
                else if (value == 4)
                {
                    character.Condition.BaseValue++;
                    character.Healing.BaseValue = character.Healing.StartValue;
                    character.Defence.BaseValue = character.Defence.StartValue;
                    character.HPstart.BaseValue = character.HPstart.StartValue;
                    character.HPlvl.BaseValue = character.HPlvl.StartValue;
                    character.Poisoning.BaseValue = character.Poisoning.StartValue;
                    character.Dazzle.BaseValue = character.Dazzle.StartValue;
                    statPoints--;


                }
                else if (value >= 5)
                {
                    character.Condition.BaseValue++;
                    character.Healing.BaseValue += character.Healing.ValuePerPoint1;
                    character.Defence.BaseValue += character.Defence.ValuePerPoint2;
                    character.HPstart.BaseValue += character.HPstart.ValuePerPoint1;
                    character.HPlvl.BaseValue += character.HPlvl.ValuePerPoint1;
                    character.Poisoning.BaseValue += character.Poisoning.ValuePerPoint1;
                    character.Dazzle.BaseValue += character.Dazzle.ValuePerPoint1;
                    statPoints--;


                }

            }
            else
            {
                Debug.Log("Nie masz punktow statystyk");
            }
        }
        public void AddInt()
        {
            if (statPoints > 0)
            {
                Debug.Log("dodaje inteligencjii");

                float value = character.Intelligence.BaseValue;

                if (value == 3)
                {
                    character.Intelligence.BaseValue++;
                    character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValuMinusOne;
                    character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValuMinusOne;
                    character.SPlvl.BaseValue = character.SPlvl.ValuMinusOne;
                    character.SPstart.BaseValue = character.SPstart.ValuMinusOne;
                    statPoints--;



                }
                else if (value == 4)
                {
                    character.Intelligence.BaseValue++;
                    character.ShotAccuracy.BaseValue = character.ShotAccuracy.StartValue;
                    character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.StartValue;
                    character.SPstart.BaseValue = character.SPstart.StartValue;
                    character.SPlvl.BaseValue = character.SPlvl.StartValue;
                    statPoints--;


                }
                else if (value >= 5)
                {
                    character.Intelligence.BaseValue++;
                    character.ShotAccuracy.BaseValue += character.ShotAccuracy.ValuePerPoint2;
                    character.ThrowingAccuracy.BaseValue += character.ThrowingAccuracy.ValuePerPoint2;
                    character.SPlvl.BaseValue += character.SPlvl.ValuePerPoint1;
                    character.SPstart.BaseValue += character.SPstart.ValuePerPoint1;
                    statPoints--;


                }

            }
            else
            {
                Debug.Log("Nie masz punktow statystyk");
            }
        }
        public void AddWil()
        {
            if (statPoints > 0)
            {
                Debug.Log("dodaje sily wolii");

                float value = character.Willpower.BaseValue;

                if (value == 3)
                {
                    character.Willpower.BaseValue++;
                    character.Hit.BaseValue = character.Hit.ValuMinusOne;
                    character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValuMinusOne;
                    character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValuMinusOne;
                    character.Healing.BaseValue = character.Healing.ValuMinusOne;
                    character.Defence.BaseValue = character.Defence.ValuMinusOne;
                    character.Sneaking.BaseValue = character.Sneaking.ValuMinusOne;
                    character.Panic.BaseValue = character.Panic.ValuMinusOne;
                    character.Demotivation.BaseValue = character.Demotivation.ValuMinusOne;
                    statPoints--;



                }
                else if (value == 4)
                {
                    character.Willpower.BaseValue++;
                    character.ShotAccuracy.BaseValue = character.ShotAccuracy.StartValue;
                    character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.StartValue;
                    character.Hit.BaseValue = character.Hit.StartValue;
                    character.Healing.BaseValue = character.Healing.StartValue;
                    character.Defence.BaseValue = character.Defence.StartValue;
                    character.Sneaking.BaseValue = character.Sneaking.StartValue;
                    character.Panic.BaseValue = character.Panic.StartValue;
                    character.Demotivation.BaseValue = character.Demotivation.StartValue;
                    statPoints--;


                }
                else if (value >= 5)
                {
                    character.Willpower.BaseValue++;
                    character.ShotAccuracy.BaseValue += character.ShotAccuracy.ValuePerPoint3;
                    character.ThrowingAccuracy.BaseValue += character.ThrowingAccuracy.ValuePerPoint3;
                    character.Hit.BaseValue += character.Hit.ValuePerPoint2;
                    character.Healing.BaseValue += character.Healing.ValuePerPoint2;
                    character.Defence.BaseValue += character.Defence.ValuePerPoint2;
                    character.Sneaking.BaseValue += character.Sneaking.ValuePerPoint2;
                    character.Panic.BaseValue += character.Panic.ValuePerPoint1;
                    character.Demotivation.BaseValue += character.Demotivation.ValuePerPoint1;
                    statPoints--;


                }

            }
            else
            {
                Debug.Log("Nie masz punktow statystyk");
            }
        }
        public void AddCha()
        {
            if (statPoints > 0)
            {
                Debug.Log("dodaje sily charyzmy");

                character.Charisma.BaseValue++;
                statPoints--;
            }
            else
            {
                Debug.Log("Nie masz punktow statystyk");
            }
        }
        public void SubStr()
        {

            float value = character.Strength.BaseValue;

            if (value >= 6)
            {
                Debug.Log("Odejmuje sily");
                character.Strength.BaseValue--;
                character.ShotAccuracy.BaseValue -= character.ShotAccuracy.ValuePerPoint1;
                character.Range.BaseValue -= character.Range.ValuePerPoint1;

                character.Blow.BaseValue -= character.Blow.ValuePerPoint1;
                statPoints++;

            }
            else if (value == 5)
            {
                character.Strength.BaseValue--;

                character.Blow.BaseValue = character.Blow.ValuMinusOne;
                character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValuMinusOne;
                character.Range.BaseValue = character.Range.ValuMinusOne;
                statPoints++;
            }
            else if (value == 4)
            {
                character.Strength.BaseValue--;

                character.Blow.BaseValue = character.Blow.ValueMinusTwo;
                character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValueMinusTwo;
                character.Range.BaseValue = character.Range.ValueMinusTwo;
                statPoints++;
            }
            else if (value == 3)
                Debug.Log("Nie mozna odjac wiecej sily");

            Debug.Log("Wynosi: " + character.Strength.BaseValue);
        }
        public void SubAgi()
        {

            float value = character.Agility.BaseValue;

            if (value >= 6)
            {
                Debug.Log("Odejmuje zrecznosci");
                character.Agility.BaseValue--;
                character.Hit.BaseValue -= character.Hit.ValuePerPoint1;
                character.Range.BaseValue -= character.Range.ValuePerPoint2;
                character.ThrowingAccuracy.BaseValue -= character.ThrowingAccuracy.ValuePerPoint1;
                character.Dodges.BaseValue -= character.Dodges.ValuePerPoint1;
                character.Sneaking.BaseValue -= character.Sneaking.ValuePerPoint1;
                statPoints++;

            }
            else if (value == 5)
            {
                character.Agility.BaseValue--;
                character.Hit.BaseValue = character.Hit.ValuMinusOne;
                character.Range.BaseValue = character.Range.ValuMinusOne;
                character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValuMinusOne;
                character.Dodges.BaseValue = character.Dodges.ValuMinusOne;
                character.Sneaking.BaseValue = character.Sneaking.ValuMinusOne;
                statPoints++;
            }
            else if (value == 4)
            {
                character.Agility.BaseValue--;
                character.Hit.BaseValue = character.Hit.ValueMinusTwo;
                character.Range.BaseValue = character.Range.ValueMinusTwo;
                character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValueMinusTwo;
                character.Dodges.BaseValue = character.Dodges.ValueMinusTwo;
                character.Sneaking.BaseValue = character.Sneaking.ValueMinusTwo;
                statPoints++;
            }
            else if (value == 3)
                Debug.Log("Nie mozna odjac wiecej zrecznosci");

            Debug.Log("Wynosi: " + character.Agility.BaseValue);
        }
        public void SubCon()
        {

            float value = character.Condition.BaseValue;

            if (value >= 6)
            {
                Debug.Log("Odejmuje kondycjii");
                character.Condition.BaseValue--;
                character.Healing.BaseValue -= character.Healing.ValuePerPoint1;
                character.Defence.BaseValue -= character.Defence.ValuePerPoint2;
                character.HPlvl.BaseValue -= character.HPlvl.ValuePerPoint1;
                character.HPstart.BaseValue -= character.HPstart.ValuePerPoint1;
                character.Poisoning.BaseValue -= character.Poisoning.ValuePerPoint1;
                character.Dazzle.BaseValue -= character.Dazzle.ValuePerPoint1;
                statPoints++;

            }
            else if (value == 5)
            {
                character.Condition.BaseValue--;
                character.Healing.BaseValue = character.Healing.ValuMinusOne;
                character.Defence.BaseValue = character.Defence.ValuMinusOne;
                character.HPstart.BaseValue = character.HPstart.ValuMinusOne;
                character.HPlvl.BaseValue = character.HPlvl.ValuMinusOne;
                character.Poisoning.BaseValue = character.Poisoning.ValuMinusOne;
                character.Dazzle.BaseValue = character.Dazzle.ValuMinusOne;
                statPoints++;
            }
            else if (value == 4)
            {
                character.Condition.BaseValue--;
                character.Healing.BaseValue = character.Healing.ValueMinusTwo;
                character.Defence.BaseValue = character.Defence.ValueMinusTwo;
                character.HPlvl.BaseValue = character.HPlvl.ValueMinusTwo;
                character.HPstart.BaseValue = character.HPstart.ValueMinusTwo;
                character.Poisoning.BaseValue = character.Poisoning.ValueMinusTwo;
                character.Dazzle.BaseValue = character.Dazzle.ValueMinusTwo;
                statPoints++;
            }
            else if (value == 3)
                Debug.Log("Nie mozna odjac wiecej kondycjii");

            Debug.Log("Wynosi: " + character.Condition.BaseValue);
        }
        public void SubInt()
        {

            float value = character.Intelligence.BaseValue;

            if (value >= 6)
            {
                Debug.Log("Odejmuje inteligencjii");
                character.Intelligence.BaseValue--;
                character.ThrowingAccuracy.BaseValue -= character.ThrowingAccuracy.ValuePerPoint2;
                character.ShotAccuracy.BaseValue -= character.ShotAccuracy.ValuePerPoint2;
                character.SPstart.BaseValue -= character.SPstart.ValuePerPoint1;
                character.SPlvl.BaseValue -= character.SPlvl.ValuePerPoint1;
                statPoints++;

            }
            else if (value == 5)
            {
                character.Intelligence.BaseValue--;
                character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValuMinusOne;
                character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValuMinusOne;
                character.SPlvl.BaseValue = character.SPlvl.ValuMinusOne;
                character.SPstart.BaseValue = character.SPstart.ValuMinusOne;
                statPoints++;
            }
            else if (value == 4)
            {
                character.Intelligence.BaseValue--;
                character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValueMinusTwo;
                character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValueMinusTwo;
                character.SPstart.BaseValue = character.SPstart.ValueMinusTwo;
                character.SPlvl.BaseValue = character.SPlvl.ValueMinusTwo;
                statPoints++;
            }
            else if (value == 3)
                Debug.Log("Nie mozna odjac wiecej inteligencjii");

            Debug.Log("Wynosi: " + character.Intelligence.BaseValue);
        }
        public void SubWil()
        {

            float value = character.Willpower.BaseValue;

            if (value >= 6)
            {
                Debug.Log("Odejmuje sily woli");
                character.Willpower.BaseValue--;
                character.ThrowingAccuracy.BaseValue -= character.ThrowingAccuracy.ValuePerPoint3;
                character.ShotAccuracy.BaseValue -= character.ShotAccuracy.ValuePerPoint3;
                character.Hit.BaseValue -= character.Hit.ValuePerPoint2;
                character.Healing.BaseValue -= character.Healing.ValuePerPoint2;
                character.Defence.BaseValue -= character.Defence.ValuePerPoint2;
                character.Sneaking.BaseValue -= character.Sneaking.ValuePerPoint2;
                character.Panic.BaseValue -= character.Panic.ValuePerPoint1;
                character.Demotivation.BaseValue -= character.Demotivation.ValuePerPoint1;
                statPoints++;

            }
            else if (value == 5)
            {
                character.Willpower.BaseValue--;
                character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValuMinusOne;
                character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValuMinusOne;
                character.Hit.BaseValue = character.Hit.ValuMinusOne;
                character.Healing.BaseValue = character.Healing.ValuMinusOne;
                character.Defence.BaseValue = character.Defence.ValuMinusOne;
                character.Sneaking.BaseValue = character.Sneaking.ValuMinusOne;
                character.Panic.BaseValue = character.Panic.ValuMinusOne;
                character.Demotivation.BaseValue = character.Demotivation.ValuMinusOne;
                statPoints++;
            }
            else if (value == 4)
            {
                character.Willpower.BaseValue--;
                character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.ValueMinusTwo;
                character.ShotAccuracy.BaseValue = character.ShotAccuracy.ValueMinusTwo;
                character.Hit.BaseValue = character.Hit.ValueMinusTwo;
                character.Healing.BaseValue = character.Healing.ValueMinusTwo;
                character.Defence.BaseValue = character.Defence.ValueMinusTwo;
                character.Sneaking.BaseValue = character.Sneaking.ValueMinusTwo;
                character.Panic.BaseValue = character.Panic.ValueMinusTwo;
                character.Demotivation.BaseValue = character.Demotivation.ValueMinusTwo;
                statPoints++;
            }
            else if (value == 3)
                Debug.Log("Nie mozna odjac wiecej sily woli");

            Debug.Log("Wynosi: " + character.Willpower.BaseValue);
        }

        public void SubChar()
        {
            float value = character.Charisma.BaseValue;
            if (value >= 4)
            {
                Debug.Log("Odejmuje charyzmy");
                character.Charisma.BaseValue -= 1;
                statPoints++;
                Debug.Log("Wynosi: " + character.Charisma.BaseValue);

            }
            else if (value == 3)
            {
                Debug.Log("Nie mozna odjac wiecej charyzmy");
            }
        }
        public void ApllyStats()
        {
            character.Strength.BaseValue += str;
            str = 0;
        }
    }

}
