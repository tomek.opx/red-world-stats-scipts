using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.TestTools;
using RedWorld.CharacterStats;


namespace Game.StatSystem
{
    public class PlayerStatsUI : MonoBehaviour
    {
        public GameObject strCounterObj,agiCounterObj, conCounterObj, intCounterObj, chaCounterObj, wilCounterObj, blowAddObj, hitAddObj, shoAccAddObj, rangeAddObj, throAccAddObj, healingAddObj, dodgesAddObj, defAddObj, sneakingAddObj, hpStartAddObj, hpLvlAddObj, spStartAddObj, spLvlAddObj, posioningAddObj, dazzleAddObj, panicAddObj, demotivationAddObj;
        public TMPro.TextMeshProUGUI LevelText, StrenghText, StrAddText, ConditionText,ConAddText, AgilityText, AgiAddText, InteligenceText,IntAddText, CharismaText,ChaAddText, WillpowerText,WilAddText, StatPointsText, CurrentExpText, ExpToNextLevelText, BlowText,BlowAddText, HitText, HitAddText,  ShotAccText, ShotAccAddText, RangeText, RangeAddText, ThrowAccText, ThrowAccAddText, HealingText, HealingAddText, DodgesText, DodgesAddText,  DefenceText, DefenceAddText, SneakingText, SneakingAddText,  HPStartText, HPStartAddText,  HPlvlText, HPlvlAddText,  SPstartText, SPstartAddText,  SPlvlText, SPlvlAddText,  PoisoningText, PoisoningAddText, DazzleText, DazzleAddText, PanicText, PanicAddText, DemotivText, DemotivAddText;
        [SerializeField]
        private Character character;
        [SerializeField]
        private PlayerLevel plevel;
        public int strCounter, agiCounter, conCounter, intCounter, chaCounter, wilCounter;
        public float blowCounter, hitCounter, shotAccCounter, rangeCounter, throwAccCounter, healingCounter, dodgesCounter, defCounter, sneakingCounter, hpStartCounter, hpLvlCounter, spStartCounter, spLvlCounter, poisioningCounter, dazzleCounter, panicCounter, demotivCounter;

        void Update()
        {
            if (strCounter <= 0)
            {
                strCounterObj.SetActive(false);
            }
            else
            {

                strCounterObj.SetActive(true);
            }
            if (agiCounter <= 0)
            {
                agiCounterObj.SetActive(false);
            }
            else
            {

                agiCounterObj.SetActive(true);
            }
            if (conCounter <= 0)
            {
                conCounterObj.SetActive(false);
            }
            else
            {

                conCounterObj.SetActive(true);
            }
            if (intCounter <= 0)
            {
                intCounterObj.SetActive(false);
            }
            else
            {

                intCounterObj.SetActive(true);
            }
            if (chaCounter <= 0)
            {
                chaCounterObj.SetActive(false);
            }
            else
            {

                chaCounterObj.SetActive(true);
            }
            if (wilCounter <= 0)
            {
                wilCounterObj.SetActive(false);
            }
            else
            {

                wilCounterObj.SetActive(true);
            }
            if (blowCounter <= 0)
            {

                blowAddObj.SetActive(false);
            }
            else
            {

                blowAddObj.SetActive(true);
            }
            if (hitCounter <= 0)
            {

                hitAddObj.SetActive(false);
            }
            else
            {

                hitAddObj.SetActive(true);
            }
            if (shotAccCounter <= 0)
            {

                shoAccAddObj.SetActive(false);
            }
            else
            {

                shoAccAddObj.SetActive(true);
            }
            if (rangeCounter <= 0)
            {

                rangeAddObj.SetActive(false);
            }
            else
            {

                rangeAddObj.SetActive(true);
            }
            if (throwAccCounter <= 0)
            {

                throAccAddObj.SetActive(false);
            }
            else
            {

                throAccAddObj.SetActive(true);
            }
            if (healingCounter <= 0)
            {

                healingAddObj.SetActive(false);
            }
            else
            {

                healingAddObj.SetActive(true);
            }
            if (dodgesCounter <= 0)
            {

                dodgesAddObj.SetActive(false);
            }
            else
            {

                dodgesAddObj.SetActive(true);
            }
            if (defCounter <= 0)
            {

                defAddObj.SetActive(false);
            }
            else
            {

                defAddObj.SetActive(true);
            }
            if (sneakingCounter <= 0)
            {

                sneakingAddObj.SetActive(false);
            }
            else
            {

                sneakingAddObj.SetActive(true);
            }
            if (hpStartCounter <= 0)
            {

                hpStartAddObj.SetActive(false);
            }
            else
            {

                hpStartAddObj.SetActive(true);
            }
            if (hpLvlCounter <= 0)
            {

                hpLvlAddObj.SetActive(false);
            }
            else
            {

                hpLvlAddObj.SetActive(true);
            }
            if (spStartCounter <= 0)
            {

                spStartAddObj.SetActive(false);
            }
            else
            {

                spStartAddObj.SetActive(true);
            }
            if (spLvlCounter <= 0)
            {

                spLvlAddObj.SetActive(false);
            }
            else
            {

                spLvlAddObj.SetActive(true);
            }
            if (poisioningCounter <= 0)
            {

                posioningAddObj.SetActive(false);
            }
            else
            {

                posioningAddObj.SetActive(true);
            }
            if (dazzleCounter <= 0)
            {

                dazzleAddObj.SetActive(false);
            }
            else
            {

                dazzleAddObj.SetActive(true);
            }
            if (panicCounter <= 0)
            {

                panicAddObj.SetActive(false);
            }
            else
            {

                panicAddObj.SetActive(true);
            }
            if (demotivCounter <= 0)
            {

                demotivationAddObj.SetActive(false);
            }
            else
            {

                demotivationAddObj.SetActive(true);
            }
            StrAddText.text = strCounter.ToString();
            AgiAddText.text = agiCounter.ToString();
            ConAddText.text = conCounter.ToString();
            IntAddText.text = intCounter.ToString();
            ChaAddText.text = chaCounter.ToString();
            WilAddText.text = wilCounter.ToString();
            BlowAddText.text = blowCounter.ToString();
            HitAddText.text = hitCounter.ToString();
            ShotAccAddText.text = shotAccCounter.ToString();
            RangeAddText.text = rangeCounter.ToString();
            ThrowAccAddText.text = throwAccCounter.ToString();
            HealingAddText.text = healingCounter.ToString();
            DodgesAddText.text = dodgesCounter.ToString();
            DefenceAddText.text = defCounter.ToString();
            SneakingAddText.text = sneakingCounter.ToString();
            HPlvlAddText.text = hpLvlCounter.ToString();
            HPStartAddText.text = hpStartCounter.ToString();
            SPlvlAddText.text = spLvlCounter.ToString();
            SPstartAddText.text = spStartCounter.ToString();
            PoisoningAddText.text = poisioningCounter.ToString();
            DazzleAddText.text = dazzleCounter.ToString();
            PanicAddText.text = panicCounter.ToString();
            DemotivAddText.text = demotivCounter.ToString();

            StrenghText.text = character.Strength.BaseValue.ToString();
            ConditionText.text = character.Condition.BaseValue.ToString();
            AgilityText.text = character.Agility.BaseValue.ToString();
            InteligenceText.text = character.Intelligence.BaseValue.ToString();
            CharismaText.text = character.Charisma.BaseValue.ToString();
            WillpowerText.text = character.Willpower.BaseValue.ToString();
            BlowText.text = character.Blow.BaseValue.ToString();
            BlowAddText.text = blowCounter.ToString();
            HitText.text = character.Hit.BaseValue.ToString();
            ShotAccText.text = character.ShotAccuracy.BaseValue.ToString();
            RangeText.text = character.Range.BaseValue.ToString();
            ThrowAccText.text = character.ThrowingAccuracy.BaseValue.ToString();
            HealingText.text = character.Healing.BaseValue.ToString();
            DodgesText.text = character.Dodges.BaseValue.ToString();
            DefenceText.text = character.Defence.BaseValue.ToString();
            SneakingText.text = character.Sneaking.BaseValue.ToString();
            HPStartText.text = character.HPstart.BaseValue.ToString();
            HPlvlText.text = character.HPlvl.BaseValue.ToString();
            SPstartText.text = character.SPstart.BaseValue.ToString();
            SPlvlText.text = character.SPlvl.BaseValue.ToString();
            PoisoningText.text = character.Poisoning.BaseValue.ToString();
            DazzleText.text = character.Dazzle.BaseValue.ToString();
            PanicText.text = character.Panic.BaseValue.ToString();
            DemotivText.text = character.Demotivation.BaseValue.ToString();

            LevelText.text = plevel.playerLevel.ToString();
            StatPointsText.text = plevel.statPoints.ToString();
            CurrentExpText.text = plevel.currentExp.ToString();
            ExpToNextLevelText.text = plevel.CurrentExpToNextLvl.ToString();
        }

        public void StrCounter()
        {
            if (strCounter <= 0)
                strCounterObj.SetActive(false);
            if (plevel.statPoints > 0)
                plevel.statPoints--;
            else
                return;


            strCounter++;
            blowCounter++;
            shotAccCounter += character.Blow.ValuePerPoint2;
            rangeCounter++;
        }
        public void AgiCounter()
        {
            if (agiCounter <= 0)
                agiCounterObj.SetActive(false);
            if (plevel.statPoints > 0)
                plevel.statPoints--;
            else
                return;


            agiCounter++;
            blowCounter++;
            hitCounter += character.Hit.ValuePerPoint1;
            rangeCounter += character.Range.ValuePerPoint2;
            throwAccCounter -= character.ThrowingAccuracy.ValuePerPoint1;
            dodgesCounter += character.Dodges.ValuePerPoint1;
            sneakingCounter += character.Sneaking.ValuePerPoint1;
            
        }
        public void ConCounter()
        {
            if (conCounter <= 0)
                conCounterObj.SetActive(false);
            if (plevel.statPoints > 0)
                plevel.statPoints--;
            else
                return;

            conCounter++;
            healingCounter += character.Healing.ValuePerPoint1;
            defCounter++;
            hpStartCounter += character.HPstart.ValuePerPoint1;
            hpLvlCounter++;
            poisioningCounter -= character.Poisoning.ValuePerPoint1;
            dazzleCounter -= character.Dazzle.ValuePerPoint1;
            
        }
        public void IntCounter()
        {
            if (intCounter <= 0)
                intCounterObj.SetActive(false);
            if (plevel.statPoints > 0)
                plevel.statPoints--;
            else
                return;

            intCounter++;
            shotAccCounter++;
            throwAccCounter -= character.ThrowingAccuracy.ValuePerPoint2;
            spStartCounter += character.HPstart.ValuePerPoint1;
            spLvlCounter++;
            
        }
        public void ChaCounter()
        {
            if (chaCounter <= 0)
                chaCounterObj.SetActive(false);
            if (plevel.statPoints > 0)
                plevel.statPoints--;
            else
                return;

            chaCounter++;
        }
        public void WilCounter()
        {
            if (wilCounter <= 0)
                wilCounterObj.SetActive(false);
            if (plevel.statPoints > 0)
                plevel.statPoints--;
            else
                return;

            wilCounter++;
            hitCounter++;
            shotAccCounter++;
            throwAccCounter -= character.ThrowingAccuracy.ValuePerPoint3;
            healingCounter++;
            defCounter += character.Healing.ValuePerPoint2;
            sneakingCounter += character.Sneaking.ValuePerPoint2;
            panicCounter -= character.Panic.ValuePerPoint1;
            demotivCounter-=character.Demotivation.ValuePerPoint1;

        }

        public void StatsApply()
        {
            character.Strength.BaseValue += strCounter;
            character.Agility.BaseValue += agiCounter;
            character.Condition.BaseValue += conCounter;
            character.Intelligence.BaseValue += intCounter;
            character.Charisma.BaseValue += chaCounter;
            character.Willpower.BaseValue += wilCounter;

            character.Blow.BaseValue += blowCounter;
            character.Hit.BaseValue += hitCounter;
            character.ShotAccuracy.BaseValue += shotAccCounter;
            character.Range.BaseValue += rangeCounter;
            character.ThrowingAccuracy.BaseValue -= throwAccCounter;
            character.Healing.BaseValue += healingCounter;
            character.Dodges.BaseValue += dodgesCounter;
            character.Defence.BaseValue -= defCounter;
            character.Sneaking.BaseValue += sneakingCounter;
            character.HPlvl.BaseValue += hpLvlCounter;
            character.HPstart.BaseValue += hpStartCounter;
            character.SPlvl.BaseValue += spLvlCounter;
            character.SPstart.BaseValue += spStartCounter;
            character.Poisoning.BaseValue -= poisioningCounter;
            character.Dazzle.BaseValue -= dazzleCounter;
            character.Panic.BaseValue -= panicCounter;
            character.Demotivation.BaseValue -= demotivCounter;

            CounterReset();
            StatPointsReset();
        }
        
        public void CounterReset()
        {
            strCounter = 0;
            agiCounter = 0;
            conCounter = 0;
            intCounter = 0;
            chaCounter = 0;
            wilCounter = 0;
            blowCounter = 0;
            hitCounter = 0;
            shotAccCounter = 0;
            rangeCounter = 0;
            throwAccCounter = 0;
            healingCounter = 0;
            dodgesCounter = 0;
            defCounter = 0;
            sneakingCounter = 0;
            hpLvlCounter = 0;
            hpStartCounter = 0;
            spLvlCounter = 0;
            spStartCounter = 0;
            poisioningCounter = 0;
            dazzleCounter = 0;
            panicCounter = 0;
            demotivCounter = 0;
        }
        public void StatPointsReset()
        {
            Debug.Log("Resetuje punkty");
            int counter = strCounter + agiCounter + conCounter + intCounter + chaCounter + wilCounter;
            plevel.statPoints += counter;
            CounterReset();

        }
        public void BaseStats()
        {
            character.Strength.BaseValue = 5;
            character.Agility.BaseValue = 5;
            character.Condition.BaseValue = 5;
            character.Intelligence.BaseValue =5;
            character.Charisma.BaseValue = 5;
            character.Willpower.BaseValue = 5;

            character.Blow.BaseValue = character.Blow.StartValue;
            character.Hit.BaseValue = character.Hit.StartValue;
            character.ShotAccuracy.BaseValue = character.ShotAccuracy.StartValue;
            character.Range.BaseValue = character.Range.StartValue;
            character.ThrowingAccuracy.BaseValue = character.ThrowingAccuracy.StartValue;
            character.Healing.BaseValue = character.Healing.StartValue;
            character.Dodges.BaseValue = character.Dodges.StartValue;
            character.Defence.BaseValue = character.Defence.StartValue;
            character.Sneaking.BaseValue = character.Sneaking.StartValue;
            character.HPlvl.BaseValue = character.HPlvl.StartValue;
            character.HPstart.BaseValue = character.HPstart.StartValue;
            character.SPlvl.BaseValue = character.SPlvl.StartValue;
            character.SPstart.BaseValue = character.SPstart.StartValue;
            character.Poisoning.BaseValue = character.Poisoning.StartValue;
            character.Dazzle.BaseValue = character.Dazzle.StartValue;
            character.Panic.BaseValue = character.Panic.StartValue;
            character.Demotivation.BaseValue = character.Demotivation.StartValue;

            plevel.statPoints = 10;
            plevel.baseLevel = 0;
            plevel.currentExp = 0;
            plevel.CurrentExpToNextLvl = 100;
        }
    }

}
