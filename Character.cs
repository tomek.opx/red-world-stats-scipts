using UnityEngine;
using RedWorld.CharacterStats;

namespace Game.StatSystem
{
    [CreateAssetMenu(fileName = "New Character", menuName = "Character/Character")]
    public class Character : ScriptableObject
    {
        public new string name;
        public CharacterStat Strength;
        public CharacterStat Agility;
        public CharacterStat Condition;
        public CharacterStat Intelligence;
        public CharacterStat Charisma;
        public CharacterStat Willpower;

        public CharacterAttributes Blow;
        public CharacterAttributes Hit;
        public CharacterAttributes ShotAccuracy;
        public CharacterAttributes Range;
        public CharacterAttributes ThrowingAccuracy;
        public CharacterAttributes Healing;
        public CharacterAttributes Dodges;
        public CharacterAttributes Defence;
        public CharacterAttributes Sneaking;
        public CharacterAttributes HPstart;
        public CharacterAttributes HPlvl;
        public CharacterAttributes SPstart;
        public CharacterAttributes SPlvl;
        public CharacterAttributes Poisoning;
        public CharacterAttributes Dazzle;
        public CharacterAttributes Panic;
        public CharacterAttributes Demotivation;

    }

}



