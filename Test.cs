using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.StatSystem
{
    public class Test : MonoBehaviour
    {
        [SerializeField]
        private PlayerLevel plevel;

        int int1 = 100, int2 = 300;

        public void AddExp500()
        {
            plevel.AddExp(int1);

        }
        public void AddExp1000()
        {
            plevel.AddExp(int2);

        }
        public void UpLevel()
        {
            plevel.LevelUp();
        }
    }

}
