using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RedWorld.CharacterStats
{ 
[Serializable]
public class CharacterAttributes 
    {
        public float BaseValue;

        public float StartValue;

        public float ValuMinusOne;
        public float ValueMinusTwo;

        public float ValuePerPoint1;
        public float ValuePerPoint2;
        public float ValuePerPoint3;


        public float Value
    {
        get
        {
            if (isDirty || BaseValue != lastBaseValue)
            {
                lastBaseValue = BaseValue;
                _value = CalculateFinalValue();
                isDirty = false;
            }
            return _value;
        }
    }

    protected bool isDirty = true;
    protected float _value;
    protected float lastBaseValue = float.MinValue;

    protected readonly List<AttributeModifier> attributeModifiers;
    public readonly ReadOnlyCollection<AttributeModifier> AttributeModifiers;

    public CharacterAttributes()
    {
        attributeModifiers = new List<AttributeModifier>();
        AttributeModifiers = attributeModifiers.AsReadOnly();

    }
    public CharacterAttributes(float baseValue) : this()
    {
        BaseValue = baseValue;
    }

    public virtual void AddModifier(AttributeModifier mod)
    {
        isDirty = true;
        attributeModifiers.Add(mod);
        attributeModifiers.Sort(CompareModifierOrder);
    }
    protected virtual int CompareModifierOrder(AttributeModifier a, AttributeModifier b)
    {
        if (a.Order < b.Order)
            return -1;
        else if (a.Order > b.Order)
            return 1;
        return 0;
    }
    public bool RemoveModifier(AttributeModifier mod)
    {
        if (attributeModifiers.Remove(mod))
        {
            isDirty = true;
            return true;
        }
        return false;
    }

    public bool RemoveAllModifiersFromSource(object source)
    {
        bool didRemove = false;

        for (int i = AttributeModifiers.Count - 1; 1 >= 0; i--)
        {
            if (AttributeModifiers[i].Source == source)
            {
                isDirty = true;
                didRemove = true;
                attributeModifiers.RemoveAt(i);
            }
        }
        return didRemove;
    }

    protected virtual float CalculateFinalValue()
    {
        float finalValue = BaseValue;
        float sumPrecentAdd = 0;
        
        for (int i = 0; i < AttributeModifiers.Count; i++)
        {

            AttributeModifier mod = AttributeModifiers[i];
            if (mod.Type == AttributeModType.Flat)
            {
                finalValue += AttributeModifiers[i].Value;
            }
            else if (mod.Type == AttributeModType.PrecentAdd)
            {
                sumPrecentAdd += mod.Value;

                if (i + 1 >= AttributeModifiers.Count || AttributeModifiers[i + 1].Type != AttributeModType.PrecentAdd)
                {
                    finalValue *= 1 + sumPrecentAdd;
                    sumPrecentAdd = 0;
                }
            }

            else if (mod.Type == AttributeModType.PrecentMult)
            {
                finalValue *= 1 + mod.Value;
            }
        }


        return (float)Math.Round(finalValue, 4);
    }

}
}